#include "DrawPolygon.h"
#include "Line.h"
#include <iostream>
#define PI 3.14
using namespace std;

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
    float a=PI/2,b=2*PI/3;
    for(int i=0;i<3;i++){
        DDA_Line(xc+int(R*cos(a)+0.5),yc-int(R*sin(a)+0.5),xc+int(R*cos(a+b)+0.5),yc-int(R*sin(a+b)+0.5),ren);
        a+=b;
    }
}
void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
    float a=PI/4,b=PI/2;
    for(int i=0;i<4;i++){
        DDA_Line(xc+int(R*cos(a)+0.5),yc-int(R*sin(a)+0.5),xc+int(R*cos(a+b)+0.5),yc-int(R*sin(a+b)+0.5),ren);
        a+=b;
    }
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
    float a=PI/2,b=2*PI/5;
    for(int i=0;i<5;i++){
        DDA_Line(xc+int(R*cos(a)+0.5),yc-int(R*sin(a)+0.5),xc+int(R*cos(a+b)+0.5),yc-int(R*sin(a+b)+0.5),ren);
        a+=b;
    }
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
    float a=PI/2,b=PI/3;
    for(int i=0;i<6;i++){
        DDA_Line(xc+int(R*cos(a)+0.5),yc-int(R*sin(a)+0.5),xc+int(R*cos(a+b)+0.5),yc-int(R*sin(a+b)+0.5),ren);
        a+=b;
    }
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
    float a=PI/2,b=6*PI/5;
    for(int i=0;i<5;i++){
        DDA_Line(xc+int(R*cos(a)+0.5),yc-int(R*sin(a)+0.5),xc+int(R*cos(a+b)+0.5),yc-int(R*sin(a+b)+0.5),ren);
        a+=b;
    }
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
    float a=PI/2,b=PI/5,r = R * sin(PI / 10) / sin(7 * PI / 10);
    for(int i=0;i<10;i++){
        if(i%2) DDA_Line(xc+int(r*cos(a)+0.5),yc-int(r*sin(a)+0.5),xc+int(R*cos(a+b)+0.5),yc-int(R*sin(a+b)+0.5),ren);
        else DDA_Line(xc+int(R*cos(a)+0.5),yc-int(R*sin(a)+0.5),xc+int(r*cos(a+b)+0.5),yc-int(r*sin(a+b)+0.5),ren);
        a+=b;
    }
}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
    float a=PI/2,b=PI/8,r = R * sin(PI / 8) / sin(6 * PI / 8);
    for(int i=0;i<16;i++){
        if(i%2) DDA_Line(xc+int(r*cos(a)+0.5),yc-int(r*sin(a)+0.5),xc+int(R*cos(a+b)+0.5),yc-int(R*sin(a+b)+0.5),ren);
        else DDA_Line(xc+int(R*cos(a)+0.5),yc-int(R*sin(a)+0.5),xc+int(r*cos(a+b)+0.5),yc-int(r*sin(a+b)+0.5),ren);
        a+=b;
    }
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
	float a=startAngle,b=PI/5,r = R * sin(PI / 10) / sin(7 * PI / 10);
    for(int i=0;i<10;i++){
        if(i%2) DDA_Line(xc+int(r*cos(a)+0.5),yc-int(r*sin(a)+0.5),xc+int(R*cos(a+b)+0.5),yc-int(R*sin(a+b)+0.5),ren);
        else DDA_Line(xc+int(R*cos(a)+0.5),yc-int(R*sin(a)+0.5),xc+int(r*cos(a+b)+0.5),yc-int(r*sin(a+b)+0.5),ren);
        a+=b;
    }
}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
	float a = PI / 2;
	while (r > 1) {
		DrawStarAngle(xc, yc, int(r + 0.5), a, ren);
		a = a + PI;
		r = r * sin(PI / 10) / sin(7 * PI / 10);
	}
}
