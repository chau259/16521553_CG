#include "Parapol.h"

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    int new_x;
    int new_y;

    new_x = xc + x;
    new_y = yc + y;
    SDL_RenderDrawPoint(ren, new_x, new_y);

    new_x = xc - x;
    new_y = yc + y;
    SDL_RenderDrawPoint(ren, new_x, new_y);
}
void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
    int x,y,d,p2,p4,w,h;
    SDL_GetRendererOutputSize(ren,&w,&h);
	p2 = 2 * A; p4 = p2 * 2;
	x = 0 ; y = 0;
	d = 1 - A;
	while ( x <= A && x<=w && y <= h )
	{
		Draw2Points(xc, yc, x, y,ren);
		if( d >= 0 )
		{
			y++;
			d -= p2;
		}
            x++;
            d += 2 * x + 1;
	}
	if( d == 1 ) d = 1 - p4;
	else d = 1 - p2;
	while( x<=w && y <= h )
	{
		Draw2Points(xc, yc, x, y,ren);
		if( d <= 0 )
		{
			x++;
			d += 4 * x;
		}
		y++;
		d -= p4;
	}
}

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
    int x,y,d,p2,p4,w,h;
    SDL_GetRendererOutputSize(ren,&w,&h);
	p2 = 2 * A; p4 = p2 * 2;
	x = 0 ; y = 0;
	d = 1 - A;
	while ( x <= A && x<=w && y+yc>=0 )
	{
		Draw2Points(xc, yc, x, y,ren);
		if( d >= 0 )
		{
			y--;
			d -= p2;
		}
            x++;
            d += 2 * x + 1;
	}
	if( d == 1 ) d = 1 - p4;
	else d = 1 - p2;
	while( x<=w&&y+yc>=0 )
	{
		Draw2Points(xc, yc, x, y,ren);
		if( d <= 0 )
		{
			x++;
			d += 4 * x;
		}
		y--;
		d -= p4;
	}
}
